// Require module

const express = require('express');
const path = require('path');

// Express initialise

const app = express();

var port = process.env.PORT || 8443;

const dirPath = path.join(__dirname, '/views/');


app.listen(port, function() {
    console.log('Server running at http://127.0.0.1:%s', port);
});

// create api

app.get("/",function(req,res){
    res.sendFile(dirPath + "index.html");
});


app.get('/health', (req,res)=>{
    res.send('.');
})

app.get('/api/ping', (req,res)=>{
    res.send('pong');
})
